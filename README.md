## Dependencies

All Alpine Linux packages.
- i3blocks (duh)
- wireless-tools (for iwgetid)
- acpi
- brightnessctl
- alsa-utils (for alsamixer, which is `amixer` on the command line)

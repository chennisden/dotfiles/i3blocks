#!/usr/bin/env bash
##!/bin/sh

SOUND_LEVEL=$(amixer get Master | awk -F"[][]" '/%/ { print $2 }' | awk -F"%" 'BEGIN{tot=0; i=0} {i++; tot+=$1} END{printf("%s\n", tot/i) }')
MUTED=$(amixer get Master | awk ' /%/{print ($NF=="[off]" ? 1 : 0); exit;}')

if [ "$MUTED" = "1" ]
then
	SOUND_LEVEL=none
	COLOR=#FA1E44
else
	COLOR=#426942
fi

# Full text
echo " SOUND $SOUND_LEVEL "

# Short text
echo "S: $SOUND_LEVEL"

# Color
echo "$COLOR"
